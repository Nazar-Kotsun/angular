using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DAL.Models;
using DAL.DataBase;
using WebAPILinq.Tests.Helpers;

namespace WebAPILinq.Integration.Tests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
         protected override void ConfigureWebHost(IWebHostBuilder builder)
         {
             builder.ConfigureServices(services =>
             {
             var descriptor = services.SingleOrDefault(
                 d => d.ServiceType ==
                      typeof(DbContextOptions<ProjectsLinqContext>));
         
             if (descriptor != null)
             {
                 services.Remove(descriptor);
             }
         
             // Add ApplicationDbContext using an in-memory database for testing.
             services.AddDbContext<ProjectsLinqContext>(options =>
             {
                 options.UseInMemoryDatabase("DbProjectsLinq");
             });
         
             // Build the service provider.
             var sp = services.BuildServiceProvider();
         
             // Create a scope to obtain a reference to the database
             // context (ApplicationDbContext).
             
             using (var scope = sp.CreateScope())
             {
                 var scopedServices = scope.ServiceProvider;
                 var db = scopedServices.GetRequiredService<ProjectsLinqContext>();
                
                 var logger = scopedServices
                     .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();
         
                 // Ensure the database is created.
                 db.Database.EnsureDeleted();
         
                 try
                 {
                     db.Set<ProjectModel>().AddRange(ProjectFactoryForTest.CreateProjects());
                     db.Set<UserModel>().AddRange(UserFactoryForTest.CreateUsers());
                     db.Set<TeamModel>().AddRange(TeamFactoryForTest.CreateTeams());
                     db.Set<TaskModel>().AddRange(TaskFactoryForTest.CreateTasks());
                     db.Set<PositionModel>().AddRange(PositionFactoryForTest.CreatePositions());
                     db.Set<TaskStateModel>().AddRange(TaskSateFactoryForTest.CreateTaskStates());
         
                     db.SaveChanges();
                 }
                 catch (Exception ex)
                 {
                     logger.LogError(ex, "An error occurred seeding the " +
                                         "database with test messages. Error: {Message}", ex.Message);
                 }
             }
         });

         }
    }
}