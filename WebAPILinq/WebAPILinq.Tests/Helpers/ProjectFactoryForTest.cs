using System;
using System.Collections.Generic;
using DAL.Models;

namespace WebAPILinq.Tests.Helpers
{
    public static class ProjectFactoryForTest
    {
        public static List<ProjectModel> CreateProjects()
        {
            return new List<ProjectModel>
            {
                new ProjectModel
                {
                    Id = 1,
                    Name = "Expedita amet quas id a.",
                    Description = "Ea ab omnis saepe rem vel et." +
                                  "\nIllo quaerat eos accusantium reiciendis dolores quibusdam ratione.",
                    CreatedAt = Convert.ToDateTime("2020-07-01T03:15:53.2885115+00:00"),
                    Deadline = Convert.ToDateTime("2021-02-02T00:57:55.0677911+00:00"),
                    AuthorId = 1,
                    TeamId = 1,
                    Price = 20000
                },
                new ProjectModel
                {
                    Id = 2,
                    Name = "Totam autem hic atque suscipit.",
                    Description = "Aut quia id adipisci alias non mollitia.\n" +
                                  "Alias et at quia soluta quisquam aspernatur nemo molestias.\n" +
                                  "Vel id suscipit vero ipsa repudiandae nesciunt.\n" +
                                  "Provident veritatis maiores aut.\n" +
                                  "Iste et incidunt.",
                    CreatedAt = Convert.ToDateTime("2020-06-30T23:19:09.5122629+00:00"),
                    Deadline = Convert.ToDateTime("2021-04-30T01:56:33.3939752+00:00"),
                    AuthorId = 2,
                    TeamId = 2,
                    Price = 1000
                },
                new ProjectModel
                {
                    Id = 3,
                    Name = "Expedita amet quas id a. Alias et at quia soluta quisquam aspernatur nemo molestias",
                    Description = "Ea ab omnis saep  Alias et at quia soluta quisquam aspernatur nemo molestias",
                                  CreatedAt = Convert.ToDateTime("2019-07-01T03:15:53.2885115+00:00"),
                    Deadline = Convert.ToDateTime("2021-02-02T00:57:55.0677911+00:00"),
                    AuthorId = 1,
                    TeamId = 1,
                    Price = 20000
                }
            };
        }
    }
}