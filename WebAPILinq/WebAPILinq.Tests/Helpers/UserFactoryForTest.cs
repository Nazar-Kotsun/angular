using System;
using System.Collections.Generic;
using DAL.Models;

namespace WebAPILinq.Tests.Helpers
{
    public static class UserFactoryForTest
    {
        public static List<UserModel> CreateUsers()
        {
            return new List<UserModel>
            {
                new UserModel
                {
                    Id = 1,
                    FirstName = "Jordane",
                    LastName = "Walker",
                    Email = "Jordane.Walker@gmail.com",
                    Birthday = Convert.ToDateTime("2005-10-28T21:26:53.0193606+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-24T09:11:10.5691173+00:00"),
                    TeamId = 1,
                    PositionId = 1
                },
                new UserModel
                {

                    Id = 2,
                    FirstName = "Mabelle",
                    LastName = "Miller",
                    Email = "Mabelle.Miller12@hotmail.com",
                    Birthday = Convert.ToDateTime("2011-09-29T15:28:43.7369629+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-22T12:35:42.5834106+00:00"),
                    TeamId = 2,
                    PositionId = 2
                },
                new UserModel
                {
                    Id = 3,
                    FirstName = "Kolby",
                    LastName = "Jones",
                    Email = "Kolby_Jones@yahoo.com",
                    Birthday = Convert.ToDateTime("2003-01-23T10:37:51.9412291+00:00"),
                    RegisteredAt = Convert.ToDateTime("2020-06-27T20:20:55.7940233+00:00"),
                    TeamId = 1,
                    PositionId = 4
                }
            };
        }
    }
}