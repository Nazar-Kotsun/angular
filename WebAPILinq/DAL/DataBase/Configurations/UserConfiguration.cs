using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DAL.ModelBuilderExtension;

namespace DAL.DataBase.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<UserModel>
    {
        public void Configure(EntityTypeBuilder<UserModel> builder)
        {
            builder.Property(u => u.FirstName).HasMaxLength(550);
            builder.Property(u => u.LastName).HasMaxLength(650);
            builder.Property(u => u.Email).IsRequired();
            builder.Property(u => u.Birthday).IsRequired();
            builder.Property(u => u.RegisteredAt).IsRequired();
            builder.Property(u => u.PositionId).IsRequired();

            builder.HasOne(u => u.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(u => u.TeamId).OnDelete(DeleteBehavior.SetNull);
            
            builder.HasData(UserModelFactory.CreateUsers());

        }
    }
}