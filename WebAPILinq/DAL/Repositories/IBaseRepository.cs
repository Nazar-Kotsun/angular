using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DAL.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        ValueTask<EntityEntry<T>> Create(T entity);
        Task<T> GetById(int id);
        Task<List<T>> GetAll();
        Task Update(T entity);
        Task Delete(int id);
    }
}