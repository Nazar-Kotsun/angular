namespace DAL.Models
{
    public enum TaskState
    {
        Created = 1, Started, Finished, Canceled
    }
}