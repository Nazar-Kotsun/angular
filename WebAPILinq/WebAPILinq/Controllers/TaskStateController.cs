using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;


namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class TaskStateController : Controller
    {
        private readonly ITaskStateService _taskStateService;

        public TaskStateController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        [HttpGet]
        [Route("taskStates")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _taskStateService.GetAllTaskStates();
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("taskStates")]
        public async Task<IActionResult> AddTaskStates([FromBody] TaskStateModel taskStateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }

            await _taskStateService.AddTaskState(taskStateModel);
            return Created("", "State was created!");
        }
        
        [HttpDelete]
        [Route("taskStates/{taskStateId}")]
        public async Task<IActionResult> DeleteTaskState(int taskStateId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _taskStateService.DeleteTaskState(taskStateId);
            return Ok("State was deleted");
        }
        
        [HttpPut]
        [Route("taskStates")]
        public async Task<IActionResult> UpdateTaskStates(TaskStateModel taskStateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }

            await _taskStateService.UpdateTaskState(taskStateModel);
            return Ok("State was updated");
        }
        
    }
}