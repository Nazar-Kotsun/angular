using System;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class ProjectController : Controller
    {
        
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        [Route("projects")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _projectService.GetAllProjects();
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("projects/projectsWithTasks/{userId}")]

        public async Task<IActionResult> GetProjectsWithTasks(int userId)
        {
            var result = await _projectService.GetProjectsWithCountOfTasksByUserId(userId);
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound("This user doesn't exist or doesn't have any projects");
            }
        }
        
        [HttpGet]
        [Route("projects/detailedInfo")]
        public async Task<IActionResult> GetProjectsDetaildeInfo()
        {
            var result = await _projectService.GetProjectsDetailed();
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("projects")]
        public async Task<IActionResult> AddProject([FromBody] ProjectModel projectModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _projectService.AddProject(projectModel);
            return Created("", projectModel);
        }
        
        [HttpDelete]
        [Route("projects/{projectId}")]
        public async Task<IActionResult> DeleteProject(int projectId)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _projectService.DeleteProject(projectId);
            return Ok("Project was deleted");
        }
        
        [HttpPut]
        [Route("projects")]
        public async Task<IActionResult> UpdateProject(ProjectModel projectModel)
        {
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            await _projectService.UpdateProject(projectModel);
            return Ok("Project was updated");
        }
        
    }
}