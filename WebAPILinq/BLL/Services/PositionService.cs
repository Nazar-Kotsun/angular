using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;

namespace BLL.Services
{
    public class PositionService : IPositionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PositionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public async Task<IEnumerable<PositionModel>> GetAllPositions()
        {
            return await _unitOfWork.PositionRepository.GetAll();
        }

        public async Task AddPosition(PositionModel positionModel)
        {
            if (positionModel != null)
            {
                await _unitOfWork.PositionRepository.Create(positionModel);
                await _unitOfWork.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public async Task DeletePosition(int positionId)
        {
            if (await _unitOfWork.PositionRepository.GetById(positionId) == null)
            {
                throw new InvalidOperationException("This position doesn't exist");
            }
            
            await _unitOfWork.PositionRepository.Delete(positionId);
            await _unitOfWork.SaveChanges();
        }

        public async Task UpdatePosition(PositionModel positionModel)
        {
            if (positionModel != null)
            {
                if (await _unitOfWork.PositionRepository.GetById(positionModel.Id) == null)
                {
                    throw new InvalidOperationException("This project doesn't exist");
                }

                await _unitOfWork.PositionRepository.Update(positionModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }
        }
    }
}