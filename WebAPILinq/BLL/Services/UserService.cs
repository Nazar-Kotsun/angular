using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;

namespace BLL.Services
{
    public class UserService : IUserService
    {

        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<UserModel>> GetAllUsers()
        {
            return await _unitOfWork.UserRepository.GetAll();
        }
        public async Task<IEnumerable<UserTasksDTO>> GetUsersWithSortedTasks()
        {
            var usersModels = await GetAllUsers();
            var tasksModels = await _unitOfWork.TaskRepository.GetAll();
            
            var result = usersModels
                .GroupJoin(tasksModels 
                        .OrderByDescending(task => task.Name.Length),
                    user => user.Id,
                    task => task.PerformerId,
                    (user, tasks) => new UserTasksDTO()
                    {
                        UserId = user.Id,
                        UserFirstName = user.FirstName,
                        UserLastName = user.LastName,
                        Email = user.Email,
                        Birthday = user.Birthday,
                        UserRegisteredAt = user.RegisteredAt,
                        TeamId = user.TeamId,
                        PositionId = user.PositionId,
                        ListTasks = tasks
                    }
                ).OrderBy(user => user.UserFirstName);
            return result;
        }
        
        public async Task<UserDetailedDTO> GetDetailedInformByUserId(int userId)
        {
            var projectsModels = await _unitOfWork.ProjectRepository.GetAll();
            var tasksModels = await _unitOfWork.TaskRepository.GetAll();
            var usersModels = await _unitOfWork.UserRepository.GetAll();
            
            var projects = projectsModels.
                GroupJoin(tasksModels,
                proj => proj.Id,
                task => task.ProjectId,
                (proj, tasks) => new ProjectModel()
                {
                    Id = proj.Id,
                    Name = proj.Name,
                    Description = proj.Description,
                    AuthorId = proj.AuthorId,
                    CreatedAt = proj.CreatedAt,
                    Deadline = proj.Deadline,
                    Price = proj.Price,
                    Tasks = tasks.ToList()
                });
            
            UserDetailedDTO result = null;
            try
            {
                //IEnumerable<TaskModel> tasks = await _unitOfWork.TaskRepository.GetAll();
                
                result = new UserDetailedDTO
                {
                    User = usersModels
                        .FirstOrDefault(user => user.Id == userId),
                    LastProject = projects.Where(project => project.AuthorId == userId)
                        .OrderByDescending(project => project.CreatedAt).FirstOrDefault(),
                    CountOfTasks = projects.Where(project => project.AuthorId == userId)
                        .OrderByDescending(project => project.CreatedAt).FirstOrDefault().Tasks.ToList().Count,
                    CountOfStartedOrCanceledTasks = tasksModels
                        .Count(task => task.PerformerId == userId &&
                                       (task.TaskStateId == (int)TaskState.Started || task.TaskStateId == (int)TaskState.Canceled)),
                    TheLongestTask = tasksModels.Where(task => task.PerformerId == userId)
                        .OrderByDescending(task => task.FinishedAt - task.CreatedAt)
                        .FirstOrDefault()
                };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("This user doesn't have any projects");
            }

            return result;
        }
        
        public async Task AddUser(UserModel user)
        {
            if (user != null)
            {
                if (user.TeamId != null)
                {
                    object obj = user.TeamId;
                    if (await _unitOfWork.TeamRepository.GetById((int) obj) == null)
                    {
                        throw new InvalidOperationException("This team doesn't exist");
                    }
                }
                
                if (await _unitOfWork.PositionRepository.GetById(user.PositionId) == null)
                {
                        throw new InvalidOperationException("This position doesn't exist");
                }
                
                 
                await _unitOfWork.UserRepository.Create(user);
                await _unitOfWork.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("User is null");
            }
        }

        public async Task DeleteUser(int userId)
        {
            if (await _unitOfWork.UserRepository.GetById(userId) == null)
            {
                throw new InvalidOperationException("This user doesn't exist");
            }
            
            await _unitOfWork.UserRepository.Delete(userId);
            await _unitOfWork.SaveChanges();
        }

        public async Task UpdateUser(UserModel userModel)
        {
            if (userModel != null)
            {
                if (await _unitOfWork.UserRepository.GetById(userModel.Id) == null)
                {
                    throw new InvalidOperationException("This user doesn't exist");
                }

                if (userModel.TeamId != null)
                {
                    object obj = userModel.TeamId;
                    if (await _unitOfWork.TeamRepository.GetById((int) obj) == null)
                    {
                        throw new InvalidOperationException("This team doesn't exist");
                    }
                }
                
                await _unitOfWork.UserRepository.Update(userModel);
                await _unitOfWork.SaveChanges();
            }

            else
            {
                throw new NullReferenceException();
            }

        }
    }
}