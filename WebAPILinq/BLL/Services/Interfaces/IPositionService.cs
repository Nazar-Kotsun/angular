using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Models;


namespace BLL.Services.Interfaces
{
    public interface IPositionService
    {
        Task<IEnumerable<PositionModel>> GetAllPositions();
        Task AddPosition(PositionModel position);
        Task DeletePosition(int positionId);
        Task UpdatePosition(PositionModel position);
    }
}