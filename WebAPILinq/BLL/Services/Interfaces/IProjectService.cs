using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using DAL.Models;


namespace BLL.Services.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectModel>>  GetAllProjects();
        Task<IEnumerable<ProjectTasksDTO>> GetProjectsWithCountOfTasksByUserId(int userId);
        Task<IEnumerable<ProjectDetailedDTO>> GetProjectsDetailed();
        Task AddProject(ProjectModel project);

        Task DeleteProject(int projectId);

        Task UpdateProject(ProjectModel projectModel);
    }
}