using System;
using System.Collections.Generic;
using DAL.Models;

namespace BLL.DTO
{
    public class UserTasksDTO
    {
        public int UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime UserRegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public int PositionId { get; set; }

        public IEnumerable<TaskModel> ListTasks { get; set; }
    }
}