using System.Collections.Generic;
using DAL.Models;

namespace BLL.DTO
{
    public class TeamUsersDTO
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserModel> ListUsers { get; set; }
    }
}