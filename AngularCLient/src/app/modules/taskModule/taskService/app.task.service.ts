import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {TaskModel} from '../../../models/TaskModel';


@Injectable()
export class AppTaskService{

  currentTaskModel: TaskModel;

  baseUrl = 'https://localhost:5001/api/tasks';
  constructor(private http: HttpClient){ }

  getTasks(): Observable<TaskModel[]> {
    return this.http.get(
      this.baseUrl).pipe(
      map((data: TaskModel[]) => {
        return data;
      }), catchError( error => {
        return throwError( 'Tasks not found!' );
      })
    );
  }

  addTask(newTask: TaskModel) {

    return this.http.post(this.baseUrl, newTask, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  updateTask(changeTask: TaskModel) {

    return this.http.put(this.baseUrl, changeTask, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  deleteTask(taskId: number){
    return this.http.delete(this.baseUrl + '/' + taskId, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

}
