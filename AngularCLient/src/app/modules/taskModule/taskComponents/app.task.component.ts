import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import {TaskModel} from '../../../models/TaskModel';
import {AppTaskService} from '../taskService/app.task.service';
import {ProjectModel} from '../../../models/ProjectModel';

@Component({
  selector: 'app-task-comp',
  templateUrl: './app.task.component.html',
  styleUrls: ['./app.task.component.css'],
})
export class AppTaskComponent implements OnInit {

  taskModels: TaskModel[];
  private taskService: AppTaskService;
  constructor(appTaskService: AppTaskService) {
    this.taskService = appTaskService;
  }
  ngOnInit(): void {
    this.taskService.getTasks().subscribe(tasks => this.taskModels = tasks);
  }

  changeTask(task: TaskModel) {
    this.taskService.currentTaskModel = task;
  }

  deleteTask(taskId: number) {
    this.taskService.deleteTask(taskId).subscribe(() => { alert('Task was deleted'); window.location.reload(); },
      exception => { console.log(exception);});
  }
}
