import {Component, Input} from '@angular/core';
import { OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {AppTaskService} from '../../taskService/app.task.service';
import {TaskModel} from '../../../../models/TaskModel';

@Component({
  selector: 'app-change-task-comp',
  templateUrl: './app.change.task.component.html',
  styleUrls: ['./app.change.task.component.css'],
})
export class AppChangeTaskComponent implements ComponentCanDeactivate{

  taskService: AppTaskService;
  saved: boolean = false;
  changeForm: FormGroup;

  constructor(appTaskService: AppTaskService) {
    this.taskService = appTaskService;
    this.changeForm = new FormGroup({
      id: new FormControl(this.taskService.currentTaskModel.id, [Validators.required]),
      name: new FormControl(this.taskService.currentTaskModel.name, [Validators.required]),
      description: new FormControl(this.taskService.currentTaskModel.description, Validators.required),
      createdAt: new FormControl(this.taskService.currentTaskModel.createdAt, Validators.required),
      finishedAt: new FormControl(this.taskService.currentTaskModel.finishedAt, Validators.required),
      projectId: new FormControl(this.taskService.currentTaskModel.projectId, [Validators.required, Validators.min(1)]),
      performerId: new FormControl(this.taskService.currentTaskModel.performerId, [Validators.required, Validators.min(1)]),
      taskStateId: new FormControl(this.taskService.currentTaskModel.taskStateId, [Validators.required, Validators.min(1)])
    });
  }

  submit(task: TaskModel): void {
    this.saved = true;
    this.taskService.updateTask(task).subscribe(data => { alert(data)},
      exception => {alert(exception.message + '\n' + exception.error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.changeForm.dirty){
      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
