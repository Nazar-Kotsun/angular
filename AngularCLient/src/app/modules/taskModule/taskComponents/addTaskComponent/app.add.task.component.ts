import {Component} from '@angular/core';
import {ProjectModel} from '../../../../models/ProjectModel';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {Observable} from 'rxjs';
import {AppTaskService} from '../../taskService/app.task.service';
import {TaskModel} from '../../../../models/TaskModel';

@Component({
  selector: 'app-add-task-comp',
  templateUrl: './app.add.task.component.html',
  styleUrls: ['./app.add.task.component.css'],
})
export class AppAddTaskComponent implements ComponentCanDeactivate{

  newTaskModel: TaskModel = {} as TaskModel;
  taskService: AppTaskService;
  saved: boolean = false;

  constructor(appTaskService: AppTaskService) {
    this.taskService = appTaskService;
  }

  addForm: FormGroup = new FormGroup({
    name: new FormControl(this.newTaskModel.name, [Validators.required]),
    description: new FormControl(this.newTaskModel.description,
      Validators.required),
    createdAt: new FormControl(this.newTaskModel.createdAt, Validators.required),
    finishedAt: new FormControl(this.newTaskModel.finishedAt, Validators.required),
    projectId: new FormControl(this.newTaskModel.projectId, [Validators.required, Validators.min(1)]),
    performerId: new FormControl(this.newTaskModel.performerId, [Validators.required, Validators.min(1)]),
    taskStateId: new FormControl(this.newTaskModel.taskStateId, [Validators.required, Validators.min(1)])
  });

  submit(task: TaskModel): void {
    this.saved = true;
    this.taskService.addTask(task).subscribe(() => { alert('Task was added')},
      error => {console.log(error); alert(error.message + ' ' + error.error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.addForm.dirty){
      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
