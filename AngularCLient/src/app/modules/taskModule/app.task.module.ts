import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppTaskComponent} from './taskComponents/app.task.component';
import {AppTaskService} from './taskService/app.task.service';
import {RouterModule, Routes} from '@angular/router';
import {AppAddProjectComponent} from '../projectModule/porojectComponents/addProjectComponent/app.add.project.component';
import {ExitAboutGuard} from '../sharedModule/app.exit.guard';
import {AppChangeProjectComponent} from '../projectModule/porojectComponents/changeProjectComponent/app.change.project.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppSharedModule} from '../sharedModule/app.shared.module';
import {AppAddTaskComponent} from './taskComponents/addTaskComponent/app.add.task.component';
import {AppChangeTaskComponent} from './taskComponents/changeTaskComponent/app.change.task.component';
import {FinishedDirective} from '../sharedModule/tasksDirectives/finishedDirective';
import {CanceledDirective} from '../sharedModule/tasksDirectives/canceledDirective';
import {StartedDirective} from '../sharedModule/tasksDirectives/startedDirective';
import {CreatedDirective} from '../sharedModule/tasksDirectives/createdDirective';

const appRoutes: Routes =[
  { path: 'tasks/add', component: AppAddTaskComponent, canDeactivate: [ExitAboutGuard]},
  { path: 'tasks/change', component: AppChangeTaskComponent, canDeactivate: [ExitAboutGuard]}];

@NgModule({
  declarations: [
    AppTaskComponent, AppAddTaskComponent, AppChangeTaskComponent, FinishedDirective, CanceledDirective,
    StartedDirective, CreatedDirective
  ],
  imports: [
    BrowserModule, HttpClientModule, RouterModule.forRoot(appRoutes), ReactiveFormsModule, AppSharedModule
  ],
  exports: [AppTaskComponent],
  providers: [AppTaskService, ExitAboutGuard]
})
export class AppTaskModule { }
