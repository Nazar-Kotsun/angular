import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {ProjectModel} from '../../../models/ProjectModel';

@Injectable()
export class AppProjectService{

  currentProjectModel: ProjectModel;

  baseUrl = 'https://localhost:5001/api/projects';
  constructor(private http: HttpClient){ }

  getProjects(): Observable<ProjectModel[]> {
    return this.http.get(
      this.baseUrl).pipe(
      map((data: ProjectModel[]) => {
        return data;
      }), catchError( error => {
        return throwError(error);
      })
    );
  }

  addProject(newProject: ProjectModel) {

    return this.http.post(this.baseUrl, newProject).pipe(map((data: ProjectModel) => {
      return data;
    }),
      catchError( error => {
        return throwError(error);
      })
    );
  }

  updateProject(changeProject: ProjectModel) {

    return this.http.put(this.baseUrl, changeProject, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  deleteProject(projectId: number){
    return this.http.delete(this.baseUrl + '/' + projectId).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }
}
