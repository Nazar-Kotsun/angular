import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppProjectComponent} from './porojectComponents/app.project.component';
import {AppProjectService} from './projectService/app.project.service';
import {HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import {AppTaskComponent} from '../taskModule/taskComponents/app.task.component';
import {AppUserComponent} from '../userModule/userComponents/app.user.component';
import {AppTeamComponent} from '../teamModule/teamComponents/app.team.component';
import {AppTaskStateComponent} from '../taskStateModule/taskStateComponents/app.task.state.component';
import {AppPositionComponent} from '../positionModule/positionComponents/app.position.component';
import {AppAddProjectComponent} from './porojectComponents/addProjectComponent/app.add.project.component';
import { ReactiveFormsModule } from '@angular/forms';
import {AppChangeProjectComponent} from './porojectComponents/changeProjectComponent/app.change.project.component';
import {AppSharedModule} from '../sharedModule/app.shared.module';
import {ExitAboutGuard} from '../sharedModule/app.exit.guard';


const appRoutes: Routes =[
  { path: 'projects/add', component: AppAddProjectComponent, canDeactivate: [ExitAboutGuard]},
  { path: 'projects/change', component: AppChangeProjectComponent, canDeactivate: [ExitAboutGuard]}];

@NgModule({
  declarations: [
    AppProjectComponent, AppAddProjectComponent, AppChangeProjectComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, RouterModule.forRoot(appRoutes), ReactiveFormsModule, AppSharedModule
  ],
  exports: [AppProjectComponent, AppAddProjectComponent, AppChangeProjectComponent],
  providers: [AppProjectService, ExitAboutGuard]
})
export class AppProjectModule { }
