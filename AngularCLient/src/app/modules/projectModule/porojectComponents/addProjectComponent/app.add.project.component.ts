import {Component} from '@angular/core';
import { OnInit } from '@angular/core';
import {ProjectModel} from '../../../../models/ProjectModel';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppProjectService} from '../../projectService/app.project.service';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-add-project-comp',
  templateUrl: './app.add.project.component.html',
  styleUrls: ['./app.add.project.component.css'],
})
export class AppAddProjectComponent implements ComponentCanDeactivate{

  newProjectModel: ProjectModel = {} as ProjectModel;
  projectService: AppProjectService;
  saved: boolean = false;

  constructor(appProjectService: AppProjectService) {
    this.projectService = appProjectService;
  }

  addForm: FormGroup = new FormGroup({
    name: new FormControl(this.newProjectModel.name, [Validators.required]),
    description: new FormControl(this.newProjectModel.description, Validators.required),
    createdAt: new FormControl(this.newProjectModel.createdAt, Validators.required),
    deadline: new FormControl(this.newProjectModel.deadline, Validators.required),
    authorId: new FormControl(this.newProjectModel.authorId, [Validators.required, Validators.min(1)]),
    teamId: new FormControl(this.newProjectModel.teamId, [Validators.required, Validators.min(1)]),
    price: new FormControl(this.newProjectModel.price, [Validators.required, Validators.min(1)])
  });

  submit(project: ProjectModel): void {
    this.saved = true;
    this.projectService.addProject(project).subscribe(() => { alert('Project was added')},
      error => {console.log(error); alert(error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.addForm.dirty){

      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
