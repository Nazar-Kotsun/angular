import {Component, Input, Output} from '@angular/core';
import {AppProjectService} from '../projectService/app.project.service';
import {ProjectModel} from '../../../models/ProjectModel';
import { OnInit, EventEmitter } from '@angular/core';
import {createModuleWithProvidersType} from '@angular/core/schematics/migrations/module-with-providers/util';
import {AppChangeProjectComponent} from './changeProjectComponent/app.change.project.component';
import {Observable} from 'rxjs';
import {ComponentCanDeactivate} from '../../sharedModule/app.exit.guard';

@Component({
  selector: 'app-project-comp',
  templateUrl: './app.project.component.html',
  styleUrls: ['./app.project.component.css'],
})
export class AppProjectComponent implements OnInit{

  projectModels: ProjectModel[];
  private projectService: AppProjectService;
  constructor(appProjectService: AppProjectService) {
    this.projectService = appProjectService;
  }

  ngOnInit(): void {
    this.projectService.getProjects().subscribe(projects => this.projectModels = projects,
        exception => { alert(exception.message + '\n' + exception.error) });
  }

  changeProject(project: ProjectModel) {
    this.projectService.currentProjectModel = project;
  }

  deleteProject(projectId: number) {
    this.projectService.deleteProject(projectId).subscribe(data => { alert('Project was deleted'); window.location.reload(); },
      exception => (console.log(exception.message + "\n" + exception.error)));
  }


}
