import {Component, Input} from '@angular/core';
import { OnInit } from '@angular/core';
import {ProjectModel} from '../../../../models/ProjectModel';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppProjectService} from '../../projectService/app.project.service';
import {Observable} from 'rxjs';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';

@Component({
  selector: 'app-change-project-comp',
  templateUrl: './app.change.project.component.html',
  styleUrls: ['./app.change.project.component.css'],
})
export class AppChangeProjectComponent implements ComponentCanDeactivate{

  projectService: AppProjectService;
  saved: boolean = false;
  changeForm: FormGroup;

  constructor(appProjectService: AppProjectService) {
    this.projectService = appProjectService;
    this.changeForm = new FormGroup({
      id: new FormControl(this.projectService.currentProjectModel.id, [Validators.required]),
      name: new FormControl(this.projectService.currentProjectModel.name, [Validators.required]),
      description: new FormControl(this.projectService.currentProjectModel.description, Validators.required),
      createdAt: new FormControl(this.projectService.currentProjectModel.createdAt, Validators.required),
      deadline: new FormControl(this.projectService.currentProjectModel.deadline, Validators.required),
      authorId: new FormControl(this.projectService.currentProjectModel.authorId, [Validators.required, Validators.min(1)]),
      teamId: new FormControl(this.projectService.currentProjectModel.teamId, [Validators.required, Validators.min(1)]),
      price: new FormControl(this.projectService.currentProjectModel.price, [Validators.required, Validators.min(1)])
    });
  }

  submit(project: ProjectModel): void {
    this.saved = true;
    this.projectService.updateProject(project).subscribe(data => { alert(data)},
      exception => {alert(exception.message + '\n' + exception.error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.changeForm.dirty){

      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
