import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {UserModel} from '../../../models/UserModel';
import {ProjectModel} from '../../../models/ProjectModel';

@Injectable()
export class AppUserService{

  currentUserModel: UserModel;
  baseUrl = 'https://localhost:5001/api/users';
  constructor(private http: HttpClient){ }

  getUsers(): Observable<UserModel[]> {
    return this.http.get(
      this.baseUrl).pipe(
      map((data: UserModel[]) => {
        return data;
      }), catchError( error => {
        return throwError( 'Users not found!' );
      })
    );
  }

  addUser(newUser: UserModel) {

    return this.http.post(this.baseUrl, newUser, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  updateUser(changeUser: UserModel) {

    return this.http.put(this.baseUrl, changeUser, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  deleteUser(userId: number){
    return this.http.delete(this.baseUrl + '/' + userId).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }
}
