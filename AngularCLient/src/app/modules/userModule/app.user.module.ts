import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppUserComponent} from './userComponents/app.user.component';
import {AppUserService} from './userService/app.user.service';
import {RouterModule, Routes} from '@angular/router';
import {AppAddProjectComponent} from '../projectModule/porojectComponents/addProjectComponent/app.add.project.component';
import {ExitAboutGuard} from '../sharedModule/app.exit.guard';
import {AppChangeProjectComponent} from '../projectModule/porojectComponents/changeProjectComponent/app.change.project.component';
import {AppSharedModule} from '../sharedModule/app.shared.module';
import {AppAddUserComponent} from './userComponents/addUserComponent/app.add.user.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppChangeUserComponent} from './userComponents/changeUserComponent/app.change.user.component';

const appRoutes: Routes =[
  { path: 'users/add', component: AppAddUserComponent, canDeactivate: [ExitAboutGuard]},
  { path: 'users/change', component: AppChangeUserComponent, canDeactivate: [ExitAboutGuard]}];

@NgModule({
  declarations: [
    AppUserComponent, AppAddUserComponent, AppChangeUserComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, ReactiveFormsModule, RouterModule.forRoot(appRoutes), AppSharedModule
  ],
  exports: [AppUserComponent],
  providers: [AppUserService, ExitAboutGuard]
})
export class AppUserModule { }
