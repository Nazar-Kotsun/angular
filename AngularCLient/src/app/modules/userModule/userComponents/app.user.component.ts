import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import {UserModel} from '../../../models/UserModel';
import {AppUserService} from '../userService/app.user.service';
import {ProjectModel} from '../../../models/ProjectModel';

@Component({
  selector: 'app-user-comp',
  templateUrl: './app.user.component.html',
  styleUrls: ['./app.user.component.css'],
})
export class AppUserComponent implements OnInit {
  userModels: UserModel[];
  private userService: AppUserService;
  constructor(appUserService: AppUserService) {
    this.userService = appUserService;
  }
  ngOnInit(): void {
    this.userService.getUsers().subscribe(users => this.userModels = users);
  }

  changeUser(user: UserModel) {
    this.userService.currentUserModel = user;
  }

  deleteUser(userId: number) {
    this.userService.deleteUser(userId).subscribe(data => { alert('User was deleted'); window.location.reload(); },
      exception => { console.log(exception.message + "\n" + exception.error); });
  }

}
