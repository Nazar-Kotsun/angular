import {Component, Input} from '@angular/core';
import { OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {AppUserModule} from '../../app.user.module';
import {AppUserService} from '../../userService/app.user.service';
import {UserModel} from '../../../../models/UserModel';

@Component({
  selector: 'app-change-user-comp',
  templateUrl: './app.change.user.component.html',
  styleUrls: ['./app.change.user.component.css'],
})
export class AppChangeUserComponent implements ComponentCanDeactivate{

  userService: AppUserService;
  saved: boolean = false;
  changeForm: FormGroup;

  constructor(appUserService: AppUserService) {
    this.userService = appUserService;
      this.changeForm = new FormGroup({
        id: new FormControl(this.userService.currentUserModel.id),
      firstName: new FormControl(this.userService.currentUserModel.firstName, [Validators.required]),
      lastName: new FormControl(this.userService.currentUserModel.lastName, Validators.required),
      email: new FormControl(this.userService.currentUserModel.email, [Validators.required, Validators.email]),
      birthday: new FormControl(this.userService.currentUserModel.birthday, Validators.required),
      registeredAt: new FormControl(this.userService.currentUserModel.registeredAt, [Validators.required]),
      teamId: new FormControl(this.userService.currentUserModel.teamId, [Validators.required, Validators.min(1)]),
      positionId: new FormControl(this.userService.currentUserModel.positionId, [Validators.required, Validators.min(1)])
    });
  }

  submit(user: UserModel): void {
    this.saved = true;
    this.userService.updateUser(user).subscribe(data => { alert(data)},
      exception => {alert(exception.message + '\n' + exception.error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.changeForm.dirty){

      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
