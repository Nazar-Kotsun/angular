import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {Observable} from 'rxjs';
import {UserModel} from '../../../../models/UserModel';
import {AppUserService} from '../../userService/app.user.service';

@Component({
  selector: 'app-add-user-comp',
  templateUrl: './app.add.user.component.html',
  styleUrls: ['./app.add.user.component.css'],
})
export class AppAddUserComponent implements ComponentCanDeactivate{

  newUserModel: UserModel = {} as UserModel;
  userService: AppUserService;
  saved: boolean = false;

  constructor(appUserService: AppUserService) {
    this.userService = appUserService;
  }

  addForm: FormGroup = new FormGroup({
    firstName: new FormControl(this.newUserModel.firstName, [Validators.required]),
    lastName: new FormControl(this.newUserModel.lastName, Validators.required),
    email: new FormControl(this.newUserModel.email, [Validators.required, Validators.email]),
    birthday: new FormControl(this.newUserModel.birthday, Validators.required),
    registeredAt: new FormControl(this.newUserModel.registeredAt, [Validators.required]),
    teamId: new FormControl(this.newUserModel.teamId, [Validators.required, Validators.min(1)]),
    positionId: new FormControl(this.newUserModel.positionId, [Validators.required, Validators.min(1)])
  });

  submit(user: UserModel): void {
    this.saved = true;
    this.userService.addUser(user).subscribe(() => { alert('User was added')},
      error => {console.log(error); alert(error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.addForm.dirty){
      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
