import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appDateUa'
})
export class AppDateUaPipe implements PipeTransform {
  transform(value: Date, args?: any): string {
    let date: string = '';
    // @ts-ignore
    let currentDate = new Date(value);
    date += currentDate.getDate() + ' ';
    switch (currentDate.getUTCMonth()) {
      case 0: {
        date += 'січня' + ' ';
        break;
      }
      case 1: {
        date += 'лютого' + ' ';
        break;
      }
      case 2: {
        date += 'березня' + ' ';
        break;
      }
      case 3: {
        date += 'квітня' + ' ';
        break;
      }
      case 4: {
        date += 'травня' + ' ';
        break;
      }
      case 5: {
        date += 'червня' + ' ';
        break;
      }
      case 6: {
        date += 'липня' + ' ';
        break;
      }
      case 7: {
        date += 'серпня' + ' ';
        break;
      }
      case 8: {
        date += 'вересня' + ' ';
        break;
      }
      case 9: {
        date += 'жовтня' + ' ';
        break;
      }
      case 10: {
        date += 'листопада' + ' ';
        break;
      }
      case 11: {
        date += 'грудня' + ' ';
        break;
      }
    }

    date += currentDate.getUTCFullYear();
    return date;
  }
}
