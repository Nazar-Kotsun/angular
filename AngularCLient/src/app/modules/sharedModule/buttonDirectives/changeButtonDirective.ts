import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[changeButton]'
})
export class ChangeButtonDirective{

  constructor(private elementRef: ElementRef){
    this.elementRef.nativeElement.style.backgroundColor = "#EB9E3E";
  }
}
