import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[deleteButton]'
})
export class DeleteButtonDirective{

  constructor(private elementRef: ElementRef){
    this.elementRef.nativeElement.style.backgroundColor = "#CD3D3E";

  }
}
