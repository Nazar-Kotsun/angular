import {Directive, ElementRef, Input} from '@angular/core';
import {element} from 'protractor';

@Directive({
  selector: '[baseButton]'
})
export class BaseButtonDirective{

  constructor(private elementRef: ElementRef){
    elementRef.nativeElement.style.display = "inline-block";
    elementRef.nativeElement.style.padding = "10px";
    elementRef.nativeElement.style.borderRadius = "20px";
    elementRef.nativeElement.style.border = "2px solid black";
    elementRef.nativeElement.style.cursor = "pointer";
  }
}
