import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[addButton]'
})
export class AddButtonDirective{

  constructor(private elementRef: ElementRef){
    this.elementRef.nativeElement.style.backgroundColor = "#4EAE4A";

  }
}
