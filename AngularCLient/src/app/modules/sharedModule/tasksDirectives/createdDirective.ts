import {Directive, ElementRef, Input} from '@angular/core';

@Directive({
  selector: '[created]'
})
export class CreatedDirective{

  constructor(private elementRef: ElementRef){
    this.elementRef.nativeElement.style.display = "inline-block";
    this.elementRef.nativeElement.style.width = "50px";
    this.elementRef.nativeElement.style.height = "50px";
    this.elementRef.nativeElement.style.borderRadius = "50%";
    this.elementRef.nativeElement.style.color = "black";
    this.elementRef.nativeElement.style.fontWeight = "bold";
    this.elementRef.nativeElement.style.backgroundColor = "powderblue";

  }
}
