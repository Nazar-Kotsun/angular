import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppDateUaPipe} from './app.date.pipe';
import {BaseButtonDirective} from './buttonDirectives/baseButtonDirective';
import {DeleteButtonDirective} from './buttonDirectives/deleteButtonDirective';
import {AddButtonDirective} from './buttonDirectives/addButtonDirective';
import {ChangeButtonDirective} from './buttonDirectives/changeButtonDirective';

@NgModule({
  declarations: [
    AppDateUaPipe, BaseButtonDirective, DeleteButtonDirective, AddButtonDirective, ChangeButtonDirective
  ],
  exports: [AppDateUaPipe, BaseButtonDirective, ChangeButtonDirective, DeleteButtonDirective, AddButtonDirective]
})
export class AppSharedModule { }
