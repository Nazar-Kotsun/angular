import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppPositionComponent} from './positionComponents/app.position.component';
import {AppPositionService} from './positonService/app.position.service';


@NgModule({
  declarations: [
    AppPositionComponent
  ],
  imports: [
    BrowserModule, HttpClientModule
  ],
  exports: [AppPositionComponent],
  providers: [AppPositionService]
})
export class AppPositionModule { }
