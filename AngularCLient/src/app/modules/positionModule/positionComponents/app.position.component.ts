import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import {AppPositionService} from '../positonService/app.position.service';
import {PositionModel} from '../../../models/PositionModel';

@Component({
  selector: 'app-position-comp',
  templateUrl: './app.position.component.html',
  styleUrls: ['./app.position.component.css'],
})
export class AppPositionComponent implements OnInit {
  positionsModels: PositionModel[];
  private positionService: AppPositionService;
  constructor(appPositionService: AppPositionService) {
    this.positionService = appPositionService;
  }
  ngOnInit(): void {
    this.positionService.getPositions().subscribe(positions => this.positionsModels = positions);
  }
}
