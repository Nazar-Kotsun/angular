import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {PositionModel} from '../../../models/PositionModel';

@Injectable()
export class AppPositionService {
  baseUrl = 'https://localhost:5001/api/positions';
  constructor(private http: HttpClient){ }

  getPositions(): Observable<PositionModel[]> {
    return this.http.get(
      this.baseUrl).pipe(
      map((data: PositionModel[]) => {
        return data;
      }), catchError( error => {
        return throwError( 'Positions not found!' );
      })
    ); }
}
