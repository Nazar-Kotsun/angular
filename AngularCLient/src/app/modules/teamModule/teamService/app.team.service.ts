import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {TeamModel} from '../../../models/TeamModel';

@Injectable()
export class AppTeamService{

  currentTeamModel: TeamModel;

  baseUrl = 'https://localhost:5001/api/teams';
  constructor(private http: HttpClient){ }

  getTeams(): Observable<TeamModel[]> {
    return this.http.get(
      this.baseUrl).pipe(
      map((data: TeamModel[]) => {
        return data;
      }), catchError( error => {
        return throwError( 'Teams not found!' );
      })
    );
  }

  addTeam(newTeam: TeamModel) {

    return this.http.post(this.baseUrl, newTeam, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  updateTeam(changeTeam: TeamModel) {

    return this.http.put(this.baseUrl, changeTeam, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

  deleteTeam(teamId: number){
    return this.http.delete(this.baseUrl + '/' + teamId, {responseType: 'text'}).pipe(
      (data) => {return data},
      catchError( error => {
        return throwError(error);
      })
    );
  }

}
