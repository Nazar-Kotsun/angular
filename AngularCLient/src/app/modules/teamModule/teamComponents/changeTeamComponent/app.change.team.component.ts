import {Component, Input} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {AppTeamService} from '../../teamService/app.team.service';
import {TeamModel} from '../../../../models/TeamModel';

@Component({
  selector: 'app-change-team-comp',
  templateUrl: './app.change.team.component.html',
  styleUrls: ['./app.change.team.component.css'],
})
export class AppChangeTeamComponent implements ComponentCanDeactivate{

  teamService: AppTeamService;
  saved: boolean = false;
  changeForm: FormGroup;

  constructor(appTeamService: AppTeamService) {
    this.teamService = appTeamService;
    this.changeForm = new FormGroup({
      id: new FormControl(this.teamService.currentTeamModel.id, [Validators.required]),
      name: new FormControl(this.teamService.currentTeamModel.name, [Validators.required]),
      createdAt: new FormControl(this.teamService.currentTeamModel.createdAt, Validators.required),
    });
  }

  submit(team: TeamModel): void {
    this.saved = true;
    this.teamService.updateTeam(team).subscribe(data => { alert(data)},
      exception => {alert(exception.message + '\n' + exception.error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.changeForm.dirty){

      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
