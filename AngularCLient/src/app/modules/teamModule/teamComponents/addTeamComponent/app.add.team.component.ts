import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ComponentCanDeactivate} from '../../../sharedModule/app.exit.guard';
import {Observable} from 'rxjs';
import {TeamModel} from '../../../../models/TeamModel';
import {AppTeamService} from '../../teamService/app.team.service';

@Component({
  selector: 'app-add-team-comp',
  templateUrl: './app.add.team.component.html',
  styleUrls: ['./app.add.team.component.css'],
})
export class AppAddTeamComponent implements ComponentCanDeactivate{

  newTeamModel: TeamModel = {} as TeamModel;
  teamService: AppTeamService;
  saved: boolean = false;

  constructor(appTeamService: AppTeamService) {
    this.teamService = appTeamService;
  }

  addForm: FormGroup = new FormGroup({
    name: new FormControl(this.newTeamModel.name, Validators.required),
    createdAt: new FormControl(this.newTeamModel.createdAt, Validators.required),
  });

  submit(team: TeamModel): void {
    this.saved = true;
    alert(team.createdAt);
    this.teamService.addTeam(team).subscribe(() => { alert('Team was added')},
      error => {console.log(error); alert(error.message + "\n" + error.error)});
  }

  canDeactivate() : boolean | Observable<boolean>{

    if(!this.saved && this.addForm.dirty) {

      return confirm("Do you want to exit? You'll lose the data that was entered!");
    }
    else{
      return true;
    }
  }
}
