import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import {TeamModel} from '../../../models/TeamModel';
import {AppTeamService} from '../teamService/app.team.service';
import {ProjectModel} from '../../../models/ProjectModel';

@Component({
  selector: 'app-team-comp',
  templateUrl: './app.team.component.html',
  styleUrls: ['./app.team.component.css'],
})
export class AppTeamComponent implements OnInit {

  teamModels: TeamModel[];
  private teamService: AppTeamService;
  constructor(appTeamService: AppTeamService) {
    this.teamService = appTeamService;
  }
  ngOnInit(): void {
    this.teamService.getTeams().subscribe(teams => this.teamModels = teams);
  }

  changeTeam(team: TeamModel) {
    this.teamService.currentTeamModel = team;
  }

  deleteTeam(teamId: number) {
    this.teamService.deleteTeam(teamId).subscribe(data => { alert('Team was deleted'); window.location.reload(); },
      exception => { alert(exception.message + '\n' + exception.error)});
  }

}
