import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppTeamComponent} from './teamComponents/app.team.component';
import {AppTeamService} from './teamService/app.team.service';
import {RouterModule, Routes} from '@angular/router';
import {AppAddProjectComponent} from '../projectModule/porojectComponents/addProjectComponent/app.add.project.component';
import {ExitAboutGuard} from '../sharedModule/app.exit.guard';
import {AppChangeProjectComponent} from '../projectModule/porojectComponents/changeProjectComponent/app.change.project.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AppSharedModule} from '../sharedModule/app.shared.module';
import {AppAddTeamComponent} from './teamComponents/addTeamComponent/app.add.team.component';
import {AppChangeTeamComponent} from './teamComponents/changeTeamComponent/app.change.team.component';

const appRoutes: Routes =[
  { path: 'teams/add', component: AppAddTeamComponent, canDeactivate: [ExitAboutGuard]},
  { path: 'teams/change', component: AppChangeTeamComponent, canDeactivate: [ExitAboutGuard]}];


@NgModule({
  declarations: [
    AppTeamComponent, AppAddTeamComponent, AppChangeTeamComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, ReactiveFormsModule, RouterModule.forRoot(appRoutes), AppSharedModule
  ],
  exports: [AppTeamComponent],
  providers: [AppTeamService, ExitAboutGuard]
})
export class AppTeamModule { }
