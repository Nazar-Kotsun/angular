import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {TaskStateModel} from '../../../models/TaskStateModel';


@Injectable()
export class AppTaskStateService{

  baseUrl = 'https://localhost:5001/api/taskStates';
  constructor(private http: HttpClient){ }

  getTaskStates(): Observable<TaskStateModel[]> {
    return this.http.get(
      this.baseUrl).pipe(
      map((data: TaskStateModel[]) => {
        return data;
      }), catchError( error => {
        return throwError( 'States not found!' );
      })
    ); }
}
