import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppTaskStateComponent} from './taskStateComponents/app.task.state.component';
import {AppTaskStateService} from './taskStateService/app.task.state.service';


@NgModule({
  declarations: [
    AppTaskStateComponent
  ],
  imports: [
    BrowserModule, HttpClientModule
  ],
  exports: [AppTaskStateComponent],
  providers: [AppTaskStateService]
})
export class AppTaskStateModule { }
