import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import {TaskStateModel} from '../../../models/TaskStateModel';
import {AppTaskStateService} from '../taskStateService/app.task.state.service';

@Component({
  selector: 'app-task-state-comp',
  templateUrl: './app.task.state.component.html',
  styleUrls: ['./app.task.state.component.css'],
})
export class AppTaskStateComponent implements OnInit {
  taskStateModels: TaskStateModel[];
  private taskStateService: AppTaskStateService;
  constructor(appTaskStateService: AppTaskStateService) {
    this.taskStateService = appTaskStateService;
  }
  ngOnInit(): void {
    this.taskStateService.getTaskStates().subscribe(taskStates => this.taskStateModels = taskStates);
  }
}
