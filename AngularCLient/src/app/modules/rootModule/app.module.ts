import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent } from './AppComponent/app.component';
import {AppProjectModule} from '../projectModule/app.project.module';
import {HttpClientModule} from '@angular/common/http';
import { AppPositionModule } from '../positionModule/app.position.module';
import {AppTaskModule} from '../taskModule/app.task.module';
import {AppTaskStateModule} from '../taskStateModule/app.task.state.module';
import {AppTeamModule} from '../teamModule/app.team.module';
import {AppUserModule} from '../userModule/app.user.module';
import {AppProjectComponent} from '../projectModule/porojectComponents/app.project.component';
import {AppTaskComponent} from '../taskModule/taskComponents/app.task.component';
import {AppUserComponent} from '../userModule/userComponents/app.user.component';
import {AppTeamComponent} from '../teamModule/teamComponents/app.team.component';
import {AppTaskStateComponent} from '../taskStateModule/taskStateComponents/app.task.state.component';
import {AppPositionComponent} from '../positionModule/positionComponents/app.position.component';
import {AppSharedModule} from '../sharedModule/app.shared.module';
/*import {AppSharedModule} from '../sharedModule/app.shared.module';*/

const appRoutes: Routes =[
  { path: '', component: AppProjectComponent},
  { path: 'tasks', component: AppTaskComponent},
  { path: 'users', component: AppUserComponent},
  { path: 'teams', component: AppTeamComponent},
  { path: 'taskStates', component: AppTaskStateComponent},
  { path: 'positions', component: AppPositionComponent}
  /*{ path: '**', component: NotFoundComponent }*/
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppProjectModule,
    AppPositionModule,
    AppTaskModule,
    AppTaskStateModule,
    AppTeamModule,
    AppUserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
