export interface UserModel {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  birthday: Date;
  registeredAt: Date;
  teamId: number;
  positionId: number;
}
