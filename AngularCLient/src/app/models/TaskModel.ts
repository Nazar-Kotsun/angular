export interface TaskModel {
  id: number;
  name: string;
  description: string;
  createdAt: Date;
  finishedAt: Date;
  projectId: number;
  performerId: number;
  taskStateId: number;
}
