

export interface ProjectModel {
  id: number;
  name: string;
  description: string;
  createdAt: Date;
  deadline: Date;
  authorId: number;
  teamId: number;
  price: number;
}

