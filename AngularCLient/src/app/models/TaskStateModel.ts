export interface TaskStateModel {
  id: number;
  value: string;
}
